<?php


/*

 Register content types
 
*/

add_action( 'init', 'picto_register_post_types' );

function picto_register_post_types() {

	register_post_type(
			'membres', array(	
				'label' => __( 'Membres', 'picto' ),
				'public' => true,
				'show_ui' => true,
				'show_in_menu' => true,
				 'menu_icon' => 'dashicons-universal-access-alt', // universal-access
				// dashicons-admin-post
				'capability_type' => 'post',
				'hierarchical' => false,
				'has_archive'		 => false,
				'rewrite' => array('slug' => ''),
				'query_var' => true,
				'exclude_from_search' => false,
				'menu_position' => 6,
				'supports' => array(
					'title',
					'editor',
					'author',
					'revisions',
					),
				'labels' => array (
			  	  'name' => __( 'Membres', 'picto' ),
			  	  'singular_name' => __( 'Membre', 'picto' ),
			  	  'menu_name' => __( 'Membres', 'picto' ),
			  	  'add_new' => 'Ajouter',
			  	  'add_new_item' => 'Ajouter un Membre',
			  	  'edit' => 'Modifier',
			  	  'edit_item' => 'Modifier le Membre',
			  	  'new_item' => 'Nouveau Membre',
			  	  'view' => 'Afficher',
			  	  'view_item' => 'Afficher le Membre',
			  	  'search_items' => 'Rechercher',
			  	  'not_found' => 'Aucun résultat',
			  	  'not_found_in_trash' => 'Aucun résultat',
			  	  'parent' => 'Élément Parent',
			),
		) 
	);

}


// Remove comments from Post content.
// Also, remove featured images.

add_action( 'init', 'picto_custom_init', 10 );
function picto_custom_init() {

	remove_post_type_support( 'post', 'comments' );
	remove_post_type_support( 'post', 'trackbacks' );
	
	remove_post_type_support( 'post', 'thumbnail' );
	remove_post_type_support( 'post', 'post-formats' );
	
	// Remove Post Tags:
	register_taxonomy('post_tag', array());

}

