<?php


/*

 * Customize Editor
 * @link http://www.kevinleary.net/customizing-tinymce-wysiwyg-editor-wordpress/
 *
*/

// TinyMCE: First line toolbar customizations
if( !function_exists('base_extended_editor_mce_buttons') ){
	function picto_base_extended_editor_mce_buttons($buttons) {
		// The settings are returned in this array. Customize to suite your needs.
		return array(
			// 'formatselect', 
			'formatselect',
			'bold', 'italic', 
			// 'sub', 'sup', 
			 'bullist', 'numlist',
			'blockquote',  
			'link', 'unlink', 
			
			// 'outdent', 'indent', 
			'charmap', 
			'removeformat', 
			// 'spellchecker', 
			// 'fullscreen', 
			'wp_more', 
			'wp_help',
			// 'wp_adv'
		);
		/* WordPress Default
		return array(
			'bold', 'italic', 'strikethrough', 'separator', 
			'bullist', 'numlist', 'blockquote', 'separator', 
			'justifyleft', 'justifycenter', 'justifyright', 'separator', 
			'link', 'unlink', 'wp_more', 'separator', 
			'spellchecker', 'fullscreen', 'wp_adv'
		); */
	}
	add_filter("mce_buttons", "picto_base_extended_editor_mce_buttons", 0);
}

// TinyMCE: Second line toolbar customizations
if( !function_exists('base_extended_editor_mce_buttons_2') ){
	function picto_base_extended_editor_mce_buttons_2($buttons) {
		// The settings are returned in this array. Customize to suite your needs. An empty array is used here because I remove the second row of icons.
		return array(
//			'formatselect',
//			'pastetext', 'pasteword', 'removeformat', 'separator',
//			'undo', 'redo'
		);
		/* WordPress Default
		return array(
			'formatselect', 'underline', 'justifyfull', 'forecolor', 'separator', 
			'pastetext', 'pasteword', 'removeformat', 'separator', 
			'media', 'charmap', 'separator', 
			'outdent', 'indent', 'separator', 
			'undo', 'redo', 'wp_help'
		); */
	}
	add_filter("mce_buttons_2", "picto_base_extended_editor_mce_buttons_2", 0);
}

// Customize the format dropdown items
// See https://gist.github.com/thierrypigot/a0c69d8cc7903e742efa

add_filter('tiny_mce_before_init', 'tp_tinymce_remove_unused_formats' );
function tp_tinymce_remove_unused_formats($init)
{
  // Add block format elements you want to show in dropdown
	// $init['block_formats'] = 'Paragraph=p;Address=address;Pre=pre;Heading 1=h1;Heading 2=h2;Heading 3=h3;Heading 4=h4;Heading 5=h5;Heading 6=h6';
	$init['block_formats'] = 'Paragraph=p;Heading 2=h2;';
	return $init;

}
