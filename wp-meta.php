<?php

// Add some meta tags to the header
// PICTO - Avenue Ernest-Pictet 28-30 - 1203 Genève - © Copyright 2011 - tous droits réservés.

add_action('wp_head', 'picto_header_meta', 1);

// priority 1: before RSS feed
// priority 7: before CSS
// priority 8: before JavaScript
// priority 9: before API, RSD etc

function picto_header_meta()
{
	
	// test if frontpage
	
	if ( is_front_page() ) {
	  
	  echo '<meta name="description" content="L’association Picto est un espace de productions artistiques situé dans le quartier de la Servette à Genève." />';
	  echo "\r\n";
	  
	} else if ( is_page( 'topic-espace-dart-independant' ) ) {
		
		echo '<meta name="description" content="Topic est un espace d’art indépendant à Genève géré par des curateurs en résidence." />';
		echo "\r\n";
	
	}

	// add geographical data
		
	echo '<meta name="geo.region" content="CH-GE" />
<meta name="geo.placename" content="Gen&egrave;ve" />
<meta name="geo.position" content="46.212911;6.124542" />
<meta name="ICBM" content="46.212911, 6.124542" />';
	echo "\r\n";
}



