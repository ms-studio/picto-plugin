<?php

/*

 Load Plugin Translations
 
*/

/*
 * Force Formidable to load bundled translations.
 * See: https://wordpress.org/support/topic/how-to-use-override_load_textdomain/
 *
*/

add_filter( 'load_textdomain_mofile', 'load_custom_plugin_translation_file', 10, 2 );

function load_custom_plugin_translation_file( $mofile, $domain ) {
  $my_textdomain = 'formidable';
  if ( $my_textdomain === $domain ) {
    $mofile = WP_PLUGIN_DIR .'/'.$my_textdomain.'/languages/'.$my_textdomain.'-' . get_locale() . '.mo';
  }
  return $mofile;
}


/*

 Custom Translations
 
*/


/**
* @param string $translated
* @param string $text
* @param string $context
* @param string $domain
* @return string
*/
function picto_gettext_with_context( $translated, $text, $context, $domain ) {
    if ( 'default' == $domain ) {
        if ( 'Search &hellip;' == $text && 'placeholder' == $context ) {
            $translated = '|';
        }
    }
    return $translated;
}
add_filter( 'gettext_with_context', 'picto_gettext_with_context', 10, 4 );

/**
 *
 * @link http://codex.wordpress.org/Plugin_API/Filter_Reference/gettext
 */
function picto_gettext( $translated_text, $text, $domain ) {

    if ( is_admin() ) {
    	if ( 'acf' == $domain ) {
        if ( 'Select' == $text ) {
        	$translated_text = 'Sélectionner';
					} // text?
        } // domain?
    } // admin?
    return $translated_text;
}
add_filter( 'gettext', 'picto_gettext', 20, 4 );



/* Relabel Articles to Agenda
 ********************
*/


function bcf_replace_admin_menu_icons_css() {
    ?>
    <style>
        #menu-posts .dashicons-admin-post:before {
        	content: "\f145";
        }
    </style>
    <?php
}
add_action( 'admin_head', 'bcf_replace_admin_menu_icons_css' );