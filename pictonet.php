<?php
/*
Plugin Name: Pictonet
Plugin URI: https://github.com/ms-studio/
Description: Plugin de fonctionalité pour le site Pictonet.ch.
Version: 0.1
Author: Manuel Schmalstieg
Author URI: http://ms-studio.net
*/


// Auto-updates

// include_once (plugin_dir_path(__FILE__).'wp-auto-updates.php');

// Content Types

include_once (plugin_dir_path(__FILE__).'wp-content-types.php');

// Customize Editor

include_once (plugin_dir_path(__FILE__).'wp-editor.php');

// Translate

include_once (plugin_dir_path(__FILE__).'wp-translate.php');

// ACF fields

include_once (plugin_dir_path(__FILE__).'wp-ACF.php');

// Admin Area

include_once (plugin_dir_path(__FILE__).'wp-admin.php');

// Header Meta

include_once (plugin_dir_path(__FILE__).'wp-meta.php');

// Archives

include_once (plugin_dir_path(__FILE__).'wp-archives.php');