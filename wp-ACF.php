<?php

/**
 * Groupes de Champs pour ACF
 
 * En résumé:

 1) Galerie
 **************
 Visible pour: Membres, Articles, Pages
 Champ: 
 * acf_galerie_images
 
 2) Exposants
 **************
 Visible pour: Articles
 Champs:
 * artistes (choix de membres picto exposants)
 * artistes_exposants (texte pour exposants externes)
 
 3) Infos personnelles
 ***********************
 Visible pour: Membres
 Champs: 
 * acf_champ_activite = Champ d'activité
 * acf_num_atelier = Numéro d'atelier
 * acf_lien_externe_bloc / acf_lien_externe
 * acf_courriel
 
 4) Liens Externes
 **************************************
 Visible pour: Pages et Articles
 Champs:
 * acf_lien_externe_bloc / acf_lien_externe
 
 5) Agenda par Catégorie
  **************************************
  Visible pour: Pages
  Champs:
  * acf_agenda_cat
  
 */

if( function_exists('acf_add_local_field_group') ):


/**
 * Groupe : Galerie
 * Visible pour : Membres et Articles
 */

acf_add_local_field_group(array (
	'key' => 'group_53f4f9e459c59',
	'title' => 'Galerie',
	'fields' => array (
		array (
			'key' => 'field_51e030d9641eb',
			'label' => 'Images',
			'name' => 'acf_galerie_images',
			'type' => 'gallery',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'min' => '',
			'max' => '',
			'preview_size' => 'thumbnail',
			'insert' => 'append',
			'library' => 'all',
			'min_width' => '',
			'min_height' => '',
			'min_size' => '',
			'max_width' => '',
			'max_height' => '',
			'max_size' => '',
			'mime_types' => '',
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'membres',
			),
		),
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'post',
			),
		),
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'page',
			),
		),
	),
	'menu_order' => 0,
	// 'position' => 'normal',
	'position' => 'acf_after_title',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));


/**
 * 2)
 * Groupe : Artistes
 * Type : Relation
 * pour : Articles
 */

acf_add_local_field_group(array (
	'key' => 'group_57606d5067d09',
	'title' => 'Exposants',
	'fields' => array (
		array (
			'key' => 'field_57606d5ee9290',
			'label' => 'Membres exposants',
			'name' => 'artistes',
			'type' => 'relationship',
			'instructions' => 'Les exposants membres de Picto.',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'post_type' => array (
				0 => 'membres',
			),
			'taxonomy' => array (
			),
			'filters' => '',
			'elements' => '',
			'min' => '',
			'max' => '',
			'return_format' => 'id',
		),
		array (
			'key' => 'field_5776423dab2fc',
			'label' => 'Artistes exposants',
			'name' => 'artistes_exposants',
			'type' => 'text',
			'instructions' => 'Les exposants invités, qui ne sont pas membres de Picto. Si ce champ est renseigné, il s\'affiche à la place des membres.',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'post',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));


/**
 * 3)
 * Groupe : Infos personnelles
 * Visible pour : Membres
 */

acf_add_local_field_group(array (
	'key' => 'group_57444a4a123c6',
	'title' => 'Infos personnelles',
	'fields' => array (
		array (
					'key' => 'field_577ca64af9e5c',
					'label' => 'Champ d\'activité',
					'name' => 'acf_champ_activite',
					'type' => 'text',
					'instructions' => 'p.ex. Photographie, Illustration, Graphisme...',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
					'readonly' => 0,
					'disabled' => 0,
				),
		array (
			'key' => 'field_577ca695f9e5d',
			'label' => 'Numéro d\'atelier',
			'name' => 'acf_num_atelier',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => 'atelier',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_57444a84eac88',
			'label' => 'Liens',
			'name' => 'acf_lien_externe_bloc',
			'type' => 'repeater',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'collapsed' => '',
			'min' => '',
			'max' => '',
			'layout' => 'row',
			'button_label' => 'Ajouter un lien',
			'sub_fields' => array (
				array (
					'key' => 'field_57444a96eac89',
					'label' => 'URL',
					'name' => 'acf_lien_externe',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
					'readonly' => 0,
					'disabled' => 0,
				),
			),
		),
		array (
			'key' => 'field_574450d6f482c',
			'label' => 'Courriel',
			'name' => 'acf_courriel',
			'type' => 'email',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'membres',
			),
		),
	),
	'menu_order' => 3,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));

/*
 * Liens pour pages et articles
*/

acf_add_local_field_group(array (
	'key' => 'group_577a07179c6c7',
	'title' => 'Liens externes',
	'fields' => array (
		array (
			'key' => 'field_577a0717a33f2',
			'label' => 'Liens',
			'name' => 'acf_lien_externe_bloc',
			'type' => 'repeater',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'collapsed' => '',
			'min' => '',
			'max' => '',
			'layout' => 'row',
			'button_label' => 'Ajouter un lien',
			'sub_fields' => array (
				array (
					'key' => 'field_577a0717aa736',
					'label' => 'URL',
					'name' => 'acf_lien_externe',
					'type' => 'text',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
					'readonly' => 0,
					'disabled' => 0,
				),
			),
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'post',
			),
		),
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'page',
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));


/*
5) Agenda par Catégorie
 **************************************
 Visible pour: Pages
 Champs:
 * acf_agenda_cat
 * acf_agenda_cat_title
 */

acf_add_local_field_group(array (
	'key' => 'group_577f570731717',
	'title' => 'Agenda par catégorie',
	'fields' => array (
		array (
			'key' => 'field_577f6e01af93b',
			'label' => 'Titre (optionnel)',
			'name' => 'acf_agenda_cat_title',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_577f571b6301f',
			'label' => 'Catégorie',
			'name' => 'acf_agenda_cat',
			'type' => 'taxonomy',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'taxonomy' => 'category',
			'field_type' => 'select',
			'allow_null' => 0,
			'add_term' => 1,
			'save_terms' => 0,
			'load_terms' => 0,
			'return_format' => 'id',
			'multiple' => 0,
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'post_type',
				'operator' => '==',
				'value' => 'page',
			),
		),
	),
	'menu_order' => 20,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => 1,
	'description' => '',
));




endif;


/* Hide ACF Menu 
 * https://www.advancedcustomfields.com/resources/how-to-hide-acf-menu-from-clients/
*/
add_filter('acf/settings/show_admin', '__return_false');


