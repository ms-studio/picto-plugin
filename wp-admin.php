<?php


// Cleanup the admin interface

// User Profile: remove fields
// Method :http://wordpress.stackexchange.com/questions/94963/removing-website-field-from-the-contact-info


function remove_website_row_wpse_94963_css()
{
    echo '<style>tr.user-url-wrap{ display: none; }</style>';
}

add_action( 'admin_head-user-edit.php', 'remove_website_row_wpse_94963_css' );
add_action( 'admin_head-profile.php',   'remove_website_row_wpse_94963_css' );



add_action( 'personal_options', array ( 'T5_Hide_Profile_Bio_Box', 'start' ) );

/**
 * Captures the part with the biobox in an output buffer and removes it.
 *
 * @author Thomas Scholz, <info@toscho.de>
 *
 * http://wordpress.stackexchange.com/questions/38819/how-to-remove-biography-from-user-profile-admin-page
 */
add_action( 'personal_options', array ( 'T5_Hide_Profile_Bio_Box', 'start' ) );

class T5_Hide_Profile_Bio_Box
{
    /**
     * Called on 'personal_options'.
     *
     * @return void
     */
    public static function start()
    {
        $action = ( IS_PROFILE_PAGE ? 'show' : 'edit' ) . '_user_profile';
        add_action( $action, array ( __CLASS__, 'stop' ) );
        ob_start();
    }

    /**
     * Strips the bio box from the buffered content.
     *
     * @return void
     */
    public static function stop()
    {
        $html = ob_get_contents();
        ob_end_clean();

        // remove the headline
        
        $headline = __( IS_PROFILE_PAGE ? 'About Yourself' : 'About the user' );
        // $headline = __( IS_PROFILE_PAGE ? 'À propos de vous' : 'À propos de l’utilisateur' );
        $html = str_replace( '<h2>' . $headline . '</h2>', '', $html );

        // remove the table row
                $html = preg_replace( '~<tr class="user-description-wrap">\s*<th><label for="description".*</tr>~imsUu', '', $html );
                print $html;
    }
}


/*
 * File Upload Sanitization
 
 * Sources: 
 * http://www.geekpress.fr/wordpress/astuce/suppression-accents-media-1903/
 * https://gist.github.com/herewithme/7704370
 
 * See also Ticket #22363
 * https://core.trac.wordpress.org/ticket/22363
 * and #24661 - remove_accents is not removing combining accents
 * https://core.trac.wordpress.org/ticket/24661
*/ 

add_filter( 'sanitize_file_name', 'remove_accents', 10, 1 );
add_filter( 'sanitize_file_name_chars', 'sanitize_file_name_chars', 10, 1 );
 
function sanitize_file_name_chars( $special_chars = array() ) {
	$special_chars = array_merge( array( '’', '‘', '“', '”', '«', '»', '‹', '›', '—', 'æ', 'œ', '€','é','à','ç','ä','ö','ü','ï','û','ô','è' ), $special_chars );
	return $special_chars;
}
  

/*
 * gallery shortcode improvement
 * makes it link to the "large" file (not full size) 
 
 * http://oikos.org.uk/2011/09/tech-notes-using-resized-images-in-wordpress-galleries-and-lightboxes/
*/

function bcf_get_attachment_link_filter( $content, $post_id, $size, $permalink ) {
 
    // Only do this if we're getting the file URL
    if (! $permalink) {
        // This returns an array of (url, width, height) - options: medium, large
        $image = wp_get_attachment_image_src( $post_id, 'medium' );
        $new_content = preg_replace('/href=\'(.*?)\'/', 'href=\'' . $image[0] . '\'', $content );
        return $new_content;
    } else {
        return $content;
    }
}
 
add_filter('wp_get_attachment_link', 'bcf_get_attachment_link_filter', 10, 4);


/**
* Admin CSS 
 **/
 
 // THIS GIVES US SOME OPTIONS FOR STYLING THE ADMIN AREA
 function picto_admin_css() {
    echo '<style type="text/css">
    
            .acf-field-gallery .acf-gallery {
 							height: auto !important;
 							min-height: 230px;
            }
            
            /*
            hide gallery settings that are not applied anyway.
            */
            
            .attachments-browser .media-sidebar {
            	display: none;
            }
            
            .attachments-browser .attachments, 
            .attachments-browser .uploader-inline {
            	right: 0px;
            }
            
          </style>';
 }
 
 add_action('admin_head', 'picto_admin_css');
 
 /*
  * Login Duration
 */
 
 function picto_change_cookie_logout( $expiration, $user_id, $remember ){
     
     $expiration = ( 10 * WEEK_IN_SECONDS );
     
     return $expiration;
 }
 add_filter( 'auth_cookie_expiration','picto_change_cookie_logout', 10, 3 );
 
