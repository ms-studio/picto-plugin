<?php


/*

 Info: https://codex.wordpress.org/Configuring_Automatic_Background_Updates
 By default, every site has automatic updates enabled for minor core releases and translation files.
 
*/

add_filter( 'auto_update_plugin', '__return_true' );
add_filter( 'auto_update_theme', '__return_true' );

add_filter( 'auto_update_translation', '__return_true' );

add_filter( 'allow_minor_auto_core_updates', '__return_true' );
add_filter( 'allow_major_auto_core_updates', '__return_true' );

///